//
//  WHWeatherObject_Test.m
//  Weather_WH
//
//  Created by Vitaliy Grinevetsky on 2/5/17.
//  Copyright © 2017 Vitaliy Grinevetsky. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WHWeatherObject.h"

@interface WHWeatherObject_Test : XCTestCase

@property (nonatomic, strong) NSDictionary * json;
@end

@implementation WHWeatherObject_Test

- (void)setUp {
    [super setUp];
    [self initJSON];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}
-(void)initJSON{
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    NSString *filepath = [bundle pathForResource:@"test" ofType:@"json"];
    NSData * data = [NSData dataWithContentsOfFile:filepath];
    NSError * error = nil;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions
                                                           error:&error];
    self.json = json;
}
- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    self.json = nil;
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}
-(void)testWeatherObjectCreation{
    WHWeatherObject * object = [[WHWeatherObject alloc] initWithJSON:self.json[@"currently"]];
    XCTAssertNotNil(object.icon, @"Icon ivar nil");
    XCTAssertTrue([object.icon isEqualToString:@"clear-night"],@"Icon Not Equal");
    XCTAssertNotNil(object, @"Weather object not created");
    
}
-(void) testIconFile{
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
