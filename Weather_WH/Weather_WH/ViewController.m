//
//  ViewController.m
//  Weather_WH
//
//  Created by Vitaliy Grinevetsky on 2/5/17.
//  Copyright © 2017 Vitaliy Grinevetsky. All rights reserved.
//

#import "ViewController.h"
#import "WHDataProvider.h"
#import "WHForecast.h"
#import <JGProgressHUD/JGProgressHUD.h>

static float const kLat = 33.8688;

static float const kLong = 151.2093;

@interface ViewController ()

@property (nonatomic, strong) WHForecast * forecast;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *summaryLabel;
@property(nonatomic, strong) JGProgressHUD * hud;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self fetchData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) fetchData{
    [self showSpinner];
    CLLocation * location = [[CLLocation alloc] initWithLatitude:kLat longitude:kLong];
    __weak typeof(self) welf = self;
    [[WHDataProvider sharedManager] getWeatherDataAtLocation:location
                                                  onComplete:^(id object, NSError * error){
                                                      NSLog (@"I got it");
                                                      if (error){
                                                          [welf presentAlertWithEror:error];
                                                      }else {
                                                          WHForecast * forecast = [[WHForecast alloc] initWithJson:object];
                                                          welf.forecast = forecast;
                                                          [welf updateViews];
                                                      }
                                                      [welf hideSpinner];
                                                  }];
}
-(void) presentAlertWithEror:(NSError*)error{
    NSString * message = [NSString stringWithFormat:@"We have experienced with some issues.\n%@", [error description]?[error description]:@""];
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Error"
                                                                              message:message
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    __weak typeof(self) welf = self;
    UIAlertAction *retryAction = [UIAlertAction
                                  actionWithTitle:@"Retry"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction *action){
                                      [welf fetchData];
                                  }];
    [alertController addAction:retryAction];
    [self presentViewController:alertController
                       animated:YES
                     completion:nil];
}
-(void)updateViews{
    NSString * iconName = self.forecast.currentWeather.icon;
    self.iconImageView.image = [UIImage imageNamed:iconName];
    self.summaryLabel.text = self.forecast.currentWeather.summary;
}

#pragma mark - Spinner
-(void)showSpinner{
    self.hud = [self setupHintsHUD];
    [self.hud showInView:self.view];
}
-(void)hideSpinner{
    [self.hud dismissAnimated:YES];
    self.hud = nil;
}

-(JGProgressHUD*)setupHintsHUD
{
    JGProgressHUD *HUD = [[JGProgressHUD alloc] initWithStyle:JGProgressHUDStyleDark];
    HUD.interactionType = JGProgressHUDInteractionTypeBlockNoTouches;
    
    JGProgressHUDFadeZoomAnimation *an = [JGProgressHUDFadeZoomAnimation animation];
    HUD.animation = an;
    HUD.indicatorView = nil;
    HUD.backgroundColor = [UIColor clearColor];
    [HUD.textLabel setFont:[UIFont systemFontOfSize:17]];
    HUD.textLabel.textColor = [UIColor whiteColor];
    HUD.cornerRadius = 5;
    HUD.minimumDisplayTime = 1.5f;
    HUD.contentView.backgroundColor = [UIColor lightGrayColor];
    HUD.textLabel.textAlignment = NSTextAlignmentCenter;
    HUD.contentInsets = UIEdgeInsetsMake(15, 20, 15, 20);
    HUD.textLabel.contentMode = UIViewContentModeCenter;
    HUD.textLabel.text = @"Loading";
    return HUD;
}
@end
