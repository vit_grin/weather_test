//
//  main.m
//  Weather_WH
//
//  Created by Vitaliy Grinevetsky on 1/5/17.
//  Copyright © 2017 Vitaliy Grinevetsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
