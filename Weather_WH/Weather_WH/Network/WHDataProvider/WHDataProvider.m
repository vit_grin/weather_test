//
//  WHDataProvider.m
//  Weather_WH
//
//  Created by Vitaliy Grinevetsky on 2/5/17.
//  Copyright © 2017 Vitaliy Grinevetsky. All rights reserved.
//

#import "WHDataProvider.h"
#import <AFNetworking/AFNetworking.h>

static NSString * const kBaseUrlString = @"https://api.darksky.net/forecast";
static NSString * const kSecretAPI = @"bfd1906f1355cb120fcb5bfa4dd6580b";

@interface WHDataProvider()

@end

@implementation WHDataProvider
#pragma mark Singleton Methods

+ (id)sharedManager {
    static WHDataProvider *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    self = [super init];
    
    return self;
}


-(void) getWeatherDataAtLocation:(CLLocation*)location
                      onComplete:(void (^)(id object, NSError *error))completionHandler{
    
    
    
    AFHTTPSessionManager * sessionManager = [AFHTTPSessionManager manager];
    [sessionManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [sessionManager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [sessionManager.requestSerializer setTimeoutInterval:30];
    [sessionManager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];

    NSString * baseUrl = [NSString stringWithFormat:@"%@/%@/%f,%f", kBaseUrlString, kSecretAPI, location.coordinate.latitude, location.coordinate.longitude];
    NSURL * URL = [NSURL URLWithString:baseUrl];
    [sessionManager GET:URL.absoluteString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (completionHandler){
            completionHandler (responseObject, nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (completionHandler){
            completionHandler(nil, error);
        }
    }];
    
    
}


@end
