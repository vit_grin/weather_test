//
//  WHDataProvider.h
//  Weather_WH
//
//  Created by Vitaliy Grinevetsky on 2/5/17.
//  Copyright © 2017 Vitaliy Grinevetsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface WHDataProvider : NSObject

+ (id)sharedManager;

-(void) getWeatherDataAtLocation:(CLLocation*)location
                      onComplete:(void (^)(id object, NSError *error))completionHandler;

@end
