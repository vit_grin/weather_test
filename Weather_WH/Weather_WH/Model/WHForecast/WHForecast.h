//
//  WHForecast.h
//  Weather_WH
//
//  Created by Vitaliy Grinevetsky on 2/5/17.
//  Copyright © 2017 Vitaliy Grinevetsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WHForecast.h"
#import "WHWeatherObject.h"

@interface WHForecast : NSObject

@property (nonatomic, readonly) WHWeatherObject * currentWeather;

-(instancetype) initWithJson:(id)data;

@end
