//
//  WHForecast.m
//  Weather_WH
//
//  Created by Vitaliy Grinevetsky on 2/5/17.
//  Copyright © 2017 Vitaliy Grinevetsky. All rights reserved.
//

#import "WHForecast.h"

static NSString * const kCurrentlyKey = @"currently";

@interface WHForecast()

@property (nonatomic, strong) WHWeatherObject * currentWeather;

@end

@implementation WHForecast
-(instancetype) initWithJson:(id)data{
    self = [super init];
    if (self){
        if ([data isKindOfClass:[NSDictionary class]]){
            [self createForecastObject:data];
        }
    }
    return self;
}
-(void)createForecastObject:(NSDictionary*)dict{
    if (dict[kCurrentlyKey]){
        self.currentWeather = [[WHWeatherObject alloc] initWithJSON:dict[kCurrentlyKey]];
    }
    
}
@end
