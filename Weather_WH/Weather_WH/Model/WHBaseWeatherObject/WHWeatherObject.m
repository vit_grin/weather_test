//
//  WHBaseWeatherObject.m
//  Weather_WH
//
//  Created by Vitaliy Grinevetsky on 2/5/17.
//  Copyright © 2017 Vitaliy Grinevetsky. All rights reserved.
//

#import "WHWeatherObject.h"
//TODO: Put all constants into Constants.h file
static NSString * const kTimeKey            = @"time";
static NSString * const kSummaryKey         = @"summary";
static NSString * const kIconKey            = @"icon";
static NSString * const kSunriseTimeKey     = @"sunriseTime";
static NSString * const kSunsetTimeKey      = @"sunsetTime";
static NSString * const kTemperatureMinKey  = @"temperatureMin";
static NSString * const kTemperatureMaxKey  = @"temperatureMax";
static NSString * const kHumidityKey        = @"humidity";
static NSString * const kWindSpeedKey       = @"windSpeed";
static NSString * const kPressureKey        = @"pressure";
static NSString * const kVisibilityKey      = @"visibility";

@interface WHWeatherObject ()

@property (nonatomic, assign) NSTimeInterval time;
@property (nonatomic, assign) NSString * summary;
@property (nonatomic, assign) NSString * icon;
@property (nonatomic, assign) NSTimeInterval sunriseTime;
@property (nonatomic, assign) NSTimeInterval sunsetTime;
@property (nonatomic, assign) NSNumber * temperatureMin;
@property (nonatomic, assign) NSNumber * temperatureMax;
@property (nonatomic, assign) NSNumber * humidity;
@property (nonatomic, assign) NSNumber * windSpeed;
@property (nonatomic, assign) NSNumber * pressure;
@property (nonatomic, assign) NSNumber * visibility;

@end

@implementation WHWeatherObject

-(instancetype) initWithJSON:(NSDictionary*)data{
    self = [super init];
    if (self){
        
        [self parseParams:data];
    }
    return self;
}
-(void)parseParams:(NSDictionary*)data{
    
    if (![data isKindOfClass:[NSDictionary class]]){
        return;
    }
    NSDictionary * dict = (NSDictionary*)data;
    [self parseTime:dict];
    [self parseSummary:dict];
    [self parseIcon:dict];
    [self parseSunriseTime:dict];
    [self parseSunsetTime:dict];
    [self parseTemperature:dict isMin:YES];
    [self parseTemperature:dict isMin:NO];
    [self parseHumidity:dict];
    [self parseWindSpeed:dict];
    [self parsePressure:dict];
    [self parseVisibility:dict];
}
-(void) parseTime:(NSDictionary*)dict{
    
    NSNumber * number = [self getNumberValueFromDict:dict forKey:kTimeKey];
    self.time = number ? [number doubleValue] : -1;
}

-(void) parseSummary:(NSDictionary*)dict{
    
    self.summary = [self getStringValueFromDict:dict key:kSummaryKey];
}

-(void) parseIcon:(NSDictionary*)dict{
    
    self.icon = [self getStringValueFromDict:dict key:kIconKey];
}

-(void) parseSunriseTime:(NSDictionary*)dict{
    
    NSNumber * number = [self getNumberValueFromDict:dict forKey:kSunriseTimeKey];
    self.sunriseTime = number ? [number doubleValue] : -1;
}

-(void) parseSunsetTime:(NSDictionary*)dict{
    
    NSNumber * number = [self getNumberValueFromDict:dict forKey:kSunsetTimeKey];
    self.sunsetTime = number ? [number doubleValue] : -1;
}

-(void) parseTemperature:(NSDictionary*)dict isMin:(BOOL)isMin{
    
    NSNumber * number = nil;
    if (isMin){
        number = [self getNumberValueFromDict:dict forKey:kTemperatureMinKey];
        self.temperatureMin = number;
    }else {
        number = [self getNumberValueFromDict:dict forKey:kTemperatureMaxKey];
        self.temperatureMax = number;
    }
}

-(void) parseHumidity:(NSDictionary*)dict{
    
    NSNumber * number = [self getNumberValueFromDict:dict forKey:kHumidityKey];
    self.humidity = number;
}

-(void) parseWindSpeed:(NSDictionary*)dict{
    
    NSNumber * number = [self getNumberValueFromDict:dict forKey:kWindSpeedKey];
    self.windSpeed = number;
}

-(void) parsePressure:(NSDictionary*)dict{
    
    NSNumber * number = [self getNumberValueFromDict:dict forKey:kPressureKey];
    self.pressure = number;
}

-(void) parseVisibility:(NSDictionary*)dict{
    
    NSNumber * number = [self getNumberValueFromDict:dict forKey:kVisibilityKey];
    self.visibility = number;
}

#pragma mark - Helper Unwrap Methods
//TODO: Create category for NSDictionary
-(NSString*)getStringValueFromDict:(NSDictionary*)dict key:(NSString*)key{
    
    NSString * resValue = nil;
    if (dict[key]){
        id object = dict[key];
        if ([object isKindOfClass:[NSString class]]){
            resValue = object;
        }
    }
    return resValue;
}
-(NSNumber*)getNumberValueFromDict:(NSDictionary*)dict forKey:(NSString*)key{
    
    NSNumber * resValue = nil;
    if (dict[key]){
        id object = dict[key];
        if ([object isKindOfClass:[NSNumber class]]){
            resValue = object;
        }
    }
    return resValue;
}
@end
