//
//  WHBaseWeatherObject.h
//  Weather_WH
//
//  Created by Vitaliy Grinevetsky on 2/5/17.
//  Copyright © 2017 Vitaliy Grinevetsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WHWeatherObject : NSObject

@property (nonatomic, readonly) NSTimeInterval time;
@property (nonatomic, readonly) NSString * summary;
@property (nonatomic, readonly) NSString * icon;
@property (nonatomic, readonly) NSTimeInterval sunriseTime;
@property (nonatomic, readonly) NSTimeInterval sunsetTime;
@property (nonatomic, readonly) NSNumber * temperatureMin;
@property (nonatomic, readonly) NSNumber * temperatureMax;
@property (nonatomic, readonly) NSNumber * humidity;
@property (nonatomic, readonly) NSNumber * windSpeed;
@property (nonatomic, readonly) NSNumber * pressure;
@property (nonatomic, readonly) NSNumber * visibility;

-(instancetype) initWithJSON:(id)data;
@end
